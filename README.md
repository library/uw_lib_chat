# Library LibChat module

This module introduces some JavaScript to every page in a Drupal 7 site
that launches a SpringShare LibChat online chat widget. 

There are no dependencies for this module.

The module is maintained by Graham Faulkner g2faulkn@uwaterloo.ca.

